(setq	X	'((1 1 0 0 1 0 0 0 1 0 0 1) (1 0 1 0 0 0 1 0 1 0 1 0) (1 1 0 0 0 0 1 0 1 0 1 0) (1 1 0 0 0 0 1 1 0 0 0 1) (1 1 0 0 0 0 1 0 1 0 0 1) (1 0 0 1 1 0 0 0 1 0 1 0)))
(setq	d	'(0 0 0 0 0 1))

;;NEURAL NETWORK 1
;;--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(setq	B1	'(( 0.92 0.94 -0.42 -0.12 -0.60 -0.53) (-0.37 0.68 -0.29 0.81 0.08 0.93) (-0.86 0.02 -0.03 0.57 0.36 0.41) (-0.67 -0.52 -0.67 -0.97 -0.51 -0.73) (-0.88 0.09 0.27 0.63 -0.34 -0.40) ( 0.36 1.00 0.50 -0.82 0.29 0.76) (0.66 0.48 -0.36 -0.87 -0.46 0.52) (0.17 -0.86 0.04 0.94 0.17 -0.83) (-0.04 0.12 0.40 0.86 -0.85 -0.79) (0.79 -0.41 0.03 0.17 -0.73 -0.87) (0.17 -0.68 -0.64 -0.99 -0.05 0.02) (0.26 0.86 -0.32 0.23 -0.48 0.67)))
(setq	A1	'((0.17)(-0.89)(0.74)(-0.16)(-0.59)(-0.67)(0.47)))

;;NEURAL NETWORK 2
;;--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(setq	B2	'((-0.61 -0.69 0.48 -0.90 0.84 -0.58)( 0.77 -0.69 -0.39 0.85 -0.69 -0.73)(0.03 0.46 0.27 -0.74 0.40 -0.87)(0.76 -0.07 0.66 -0.90 0.42 -0.81)(-0.31 0.28 -0.64 0.78 0.48  0.53)(-0.58 -0.32 -0.72 0.62 -0.05  0.61)(-0.23 -0.07 -0.82 -0.67 0.56 -0.98)(0.30 0.68 0.83 -0.55 -0.25  0.82)(0.43 -0.02 -0.53 0.53 0.99  0.79)(-0.02 -0.98 -0.18 0.73 -0.13 -0.33)(0.89 0.43 0.80 -0.48 0.73 -0.19)(-0.81 -0.10 0.40 0.16 0.14 -0.57)))
(setf	A2	'((0.62)(0.21)(0.27)(-0.49)(-0.67)(-0.18)(0.27)))

;;NEURAL NETWORK 3
;;--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(setf	B3	'((0.68 0.58 -0.85 0.92 0.97 0.45)(-0.64 0.57 -0.21 0.91 0.21 0.30)(0.18 0.37 0.38 0.63 0.40 -0.26)(-0.94 -0.72 0.98  0.44 -0.16 0.38)(0.46 0.65 -0.65 -0.60 -0.97 -0.55)(0.91 0.06 -0.54 0.12 -0.49 -0.86)(0.77 0.86 0.29 0.21 0.06 0.70)(0.08 0.88 -0.04 0.08 -0.71 0.54)(-0.33 -0.81 0.06 -0.39 0.01 -0.98)(-0.09 -0.34 -0.02 -0.35 0.29 -0.54)(0.68 -0.15 -0.60 -0.19 0.83 0.88)(0.47 0.30 0.79 -0.11 0.11 -0.05)))
(setf	A3	'((0.18)(-0.86)(-0.35)(-0.57)(-0.95)(0.07)(0.67)))

;;NEURAL NETWORK 4
;;--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(setf	B4	'((0.06 0.47 0.36 -0.60 -0.69 0.29)(-0.98 -0.61 -0.22 0.71 0.18 -0.19)(0.04 0.63 -0.94 0.35 0.66 -0.84)(-0.27 0.82 -0.37 -0.83 -0.33 -0.12)(-0.13 0.34 -0.63 -0.75 0.60 -0.03)(0.30 -0.89 -0.43 0.34 0.09 -0.94)(-0.75 0.30 -0.10 0.50 0.81 -0.41)(-0.28 0.33 -0.92 -0.94 0.68 -0.37)(0.96 -0.97 0.41 0.45 -0.66 0.22)(0.85 -0.48 -0.48 0.96 -0.26 0.86)(-0.25 -0.86 -0.01 0.70 -0.64 -0.65)(-0.67 -0.64 0.32 0.89 -0.28 0.74 )))
(setf	A4	'((0.96)(-0.94)(0.03)(0.80)(0.41)(0.82)(0.16)))

;;NEURAL NETWORK 5
;;--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(setf	B5	'((0.23 0.73 0.22 0.16 0.89 -0.62)(-0.63 0.62 -0.05 -0.37 -0.53 -0.07)(-0.37 -0.62 -0.42 -0.67 0.32 -0.25)(-0.61 0.33 0.65  0.83 -0.03 0.28)(0.51 -0.16 -0.80 -0.04 0.01 0.83)(-0.25 -0.47 0.33 -0.70 -0.57 -0.74)(-0.42 0.87 -0.45 0.38 -0.60 0.37)(-0.60 0.21 -0.52 0.10 0.43 0.95)(0.09 -0.08 -0.52 0.71 0.49 0.10)(-0.07 0.32 -0.43 0.88 -1.00 -0.05)(0.28 -0.09 -0.42 -0.67 0.25 -0.84)(-0.13 0.97 -0.97 -0.78 0.26 -0.14)))
(setf	A5	'((0.49)(0.14)(0.78)(-0.62)(-0.42)(-0.05)(0.10)))

;;--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

(setf	B_MATRICES	(list B1 B2 B3 B4 B5))
(setf	A_MATRICES	(list A1 A2 A3 A4 A5))
	
(defun matrix_multiply (a b)																			;;This function was taken from:
	(flet ((col (mat i) (mapcar #'(lambda (row) (elt row i)) mat))(row (mat i) (elt mat i)))			;;https://rosettacode.org/wiki/Matrix_multiplication#Common_Lisp														
		(loop for row from 0 below (length a) collect (loop for col from 0 below (length (row b 0)) 
		collect (apply #'+ (mapcar #'* (row a row) (col b col)))))
	)
)
	

	
	
	
(defun apply_sigmoid (x init y)			;;applies the sigmoid to each value in x, 	(x = a list of lists)
	(cond
		((/= init (list-length x))		(and (setf new_x (append y (list(mapcar 'sigmoid (nth init x))))) (incf init) (apply_sigmoid x init new_x)))
		(t					 			new_x)
	)
)

(defun sigmoid (x)			;;(1 / 1 + e^x)
	(/ 1 (1+(exp x)))
)

(defun prepend (x init y)				;;prepends the number 1 to each sublist in x,	(x = a list of lists)
	(cond
		((/= init (list-length x))		(and (setf new_x (append y (list (cons 1 (nth init x))))) (incf init) (prepend x init new_x)))
		(t					 			new_x)
	)
)

(defun get_outputs (x init y)			;;creates a list of outputs
	(cond
		((/= init (list-length x))		(and (setf out (append y (nth init x))) (incf init) (get_outputs x init out)))
		(t					 			out)
	)	
)

(defun get_erro	rs (d o init l)			;;subtracts the actual outputs from the desired outputs
	(cond
		((/= init (list-length x))		(and (setf e (push (abs (- (nth init d) (nth init o))) l)) (incf init) (get_errors d o init e)))
		(t					 			(reverse e))
	)	
)

(defun sum_sqrt_errors (x)
	(expt x 2)
)


(defun fitness_function (x)				;;applies the fitness function to x 
	( - (- 100 (* (- x 1) 10)) 10)
)

(defun cumulativeReverse (x init y)		
	(cond
		((/= init (list-length x))		(and (setf new_x (push (reverse(nth init x)) y))(incf init)(cumulativeReverse x init new_x)))
		(t					 	        (reverse new_x))
	)
)

(defun cumulative (x init y)			;;adds the second element in each sublist of x to the first element of each sublist of y
	(cond
		((/= init (list-length x))		(and (setf new_x (push (list (+ (cadr (nth init x)) (caar y)) (car (nth init x)))  y))(incf init)(cumulative x init new_x)))
		(t					 	        (cumulativeReverse (cdr (reverse new_x)) 0 '()))
	)
)

(defun normalize (x init y)				;;divides the second element of each sublist of x by the total 		
	(let*									
		(
			(total	(cadar (reverse x)))	;;get the first sublist of x and takes the second element to get th total 		
		)
		
		(cond
			((/= init (list-length x))		(and (setf new_x (push (list (car (nth init x))(float (/ (cadr (nth init x)) total))) y)))(incf init)(normalize x init new_x)) 
			(t								    (reverse new_x))
		)
	)		
)

(defun neural_network (d x b a init l)
	(cond
		((/= init (list-length b))		(and 
											(setf xb 	  (matrix_multiply x (nth init b))) 					
											(setq h	 	  (prepend (apply_sigmoid xb 0 '()) 0 '()))
											(setq hA 	  (matrix_multiply h (nth init a)))
											(setf o	 	  (get_outputs (apply_sigmoid hA 0 '()) 0 '()))
											(setf e  	  (get_errors d o 0 '()))
											(setf sqrt_e  (apply '+ (mapcar 'sum_sqrt_errors e)))
											(setf fitness (fitness_function sqrt_e))
											(setf flist   (push (list (+ 1 init) fitness) l))
											(incf init) 
											(neural_network d x b a init flist)
										)
		)
		(t 			 (normalize (cumulative (sort (reverse flist) #'> :key #'second) 0 '((0))) 0 '()))
	)
)

(defun random_vals (n rand_list)						;;generates a list of n random values between 0 and 1 
	(dotimes (i n)
		(setf	rand	(float (/ (random 10) 11)))		;;divided by eleven to get more decimal places
		(push rand rand_list)
	)
	rand_list
)

(defun selection (x y x_init y_init l)
	(setf fitness  (cadr (nth x_init x)))
	(setf number   (car (nth x_init x)))
	(setf value    (nth y_init y))
	
	(if (and fitness value)
		(if (> fitness value)										;;if the fitness is greater than the random value
			(and 													;;
				(push (list number fitness) l)						;;add the fitness to a list
				(incf y_init)										;;and 
				(selection x y x_init y_init l)						;;check again with the next random number in the list y
			)
			;;else
			(and													
				(incf x_init)
				(selection x y x_init y_init l)						
			)
		)
		;;else
		(reverse l)
	)
)

(defun make_chromosomes(x b_matrices a_matrices c_list)				;;converts lists of lists to one list of 79 values
	(dotimes (i (list-length x))
		(setf index     (- (car (nth i x)) 1))
		(setf  b	    (nth index b_matrices))
		(setf  a   	    (nth index a_matrices))
		(setf  b_list   '())
		(setf  a_list   '())
		
		(dotimes (j (list-length b))							 ;;converts matrix b to a list of 72 values
			(dotimes (k (list-length (nth j b)))
				(push (nth k (nth j b)) b_list)
			)
		)
		(dotimes (l (list-length a))							;;converts matrix a to a list of 7 values
			(dotimes (m (list-length (nth l a)))
				(push (nth m (nth l a)) a_list)
			)
		)
		
		(setf chromosome (append (reverse b_list) (reverse a_list)))			;;appends list a to list b 
		(push chromosome c_list )												;;and added the new chromosome to a list
	)
	
	(reverse c_list)
)

(defun crossover (parents children alpha)								
	(setf num_of_pairs (/ (list-length parents) 2))				;;finds number of pairs from parents
	
	(dotimes (i num_of_pairs)			
		(setf p1 (nth i parents))							 
		(setf p2 (nth (+ i 1) parents))	
		(setf child '())
		
		(dotimes (j (list-length p1))
			(setf new_val (+ (* (nth j p1) alpha) (* (nth j p2) alpha)))		;;(p1 * alpha)+(p2 * alpha)
			(push new_val child)
		)
		(push (reverse child) children)
	)
	(reverse children)
)

(defun mutation (x c n)
	(setf count 0)
	(dotimes (i (list-length x))
	(setf child (nth i x))
		(dotimes (j (list-length child))																	
			(if (and (/= j 0) (= (mod j 10) 0))																;;if c is less than count 
				(if (> count c)																				;;add n to every 10th value
					(and (setf (nth (- j 1) child)   (- (nth (- j 1) child) n)) (incf count))				;;if c is greater than count
					;;else																					;;minus n from every 10th value
					(and (setf (nth (- j 1) child)   (+ (nth (- j 1) child) n)) (incf count))
				) 
			)
		)
	)
	x
)

(defun new_matrices (x b_matrix a_matrix)			;;this method isn't finished and doesn't properly 
	(setf b_rows (list-length b_matrix))			;;convert chromosomes into new weight matrices
	(setf b_cols (list-length (nth 0 b_matrix)))
	(setf b_vals (* b_rows b_cols))
	(setf a_rows (list-length a_matrix))
	(setf a_cols (list-length (nth 0 a_matrix)))
	(setf a_vals (* a_rows a_cols))

	(setf b_list  '())
	(setf a_list  '())
	(setf new_b   '())
	(setf new_a   '())
	(setf sublist '())
	
	(dotimes (i (list-length x))
		(dotimes (j b_vals)
			(push (nth j (nth i x)) b_list)
		)
		(dotimes (j a_vals)
			(push (nth (+ j b_vals) (nth i x)) a_list)
		)	
	
		(dotimes (k (list-length b_list))
			(if (or (and (/= k 0) (= (mod k b_cols) 0)) (= k 71))
				(and
					(if (= k 71)
						(push (nth k (reverse b_list)) sublist)
						;;else
						(push (reverse sublist) new_b)
					)
					(print (reverse sublist))
					(push (reverse sublist) new_b)
					(setf sublist (list (nth k (reverse b_list))))
				)
				;;else
				(push (nth k (reverse b_list)) sublist)
			)
			
		)
	)
	
	;;(print (reverse new_b))
	
)

(setf normalized_fitnesses   (neural_network d X B_MATRICES A_MATRICES 0 '()))
(setf randlist (random_vals 2 '()))
(setf chosen_fitnesses (selection normalized_fitnesses randlist 0 0 '()))
(setf parents (make_chromosomes chosen_fitnesses B_MATRICES A_MATRICES '()))
(setf children (crossover parents '() 0.6))
(setf mutated_children (mutation children 3 0.86))

(print "_______________________________________________________________________")

(print "NORMALISED FITNESSES")
(print "-----------------------------------------------------------------------")
(print normalized_fitnesses)
(print "_______________________________________________________________________")
(print "_______________________________________________________________________")


(print "RANDOM VALUES")
(print "-----------------------------------------------------------------------")
(print randlist)
(print "_______________________________________________________________________")
(print "_______________________________________________________________________")


(print "CHOSEN PARENTS")
(print "-----------------------------------------------------------------------")
(print parents)
(print "_______________________________________________________________________")
(print "_______________________________________________________________________")


(print "CROSSOVER")
(print "-----------------------------------------------------------------------")
(print children)
(print "_______________________________________________________________________")
(print "_______________________________________________________________________")


(print "MUTATION")
(print "-----------------------------------------------------------------------")
(print mutated_children)

;;(new_matrices mutated_children (nth 0 B_MATRICES) (nth 0 A_MATRICES))
